const users = require('../db_apis/users.js');
 

function getUsersFromRec(req) {
  const user = {
    username: req.username,
    email: req.email
  };
  return user
}

async function post(req, res, next) {
  try {
    let user = getUsersFromRec(req);

    user = await users.create(user)
    res.status(200).json(user)
  } catch(err) {
      next(err)
  }
}



async function get(req, res, next) {
  try {
    const context = {};
 
    context.id = parseInt(req.params.id, 10);
 
    const rows = await users.find(context);
 
    if (req.params.id) {
      if (rows.length === 1) {
        res.status(200).json(rows[0]);
      } else {
        res.status(404).end();
      }
    } else {
      res.status(200).json(rows);
    }
  } catch (err) {
    next(err);
  }
}
 
module.exports.get = get;
module.exports.post = post;