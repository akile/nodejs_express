const database = require('../services/database.js');
const oracledb = require('oracledb');
 
const baseQuery = 
 `select * from flask_test`;
 
const createSQL = `insert into flask_test (username, email) values (:username, :email) returning id into :id`

async function create(usr) {
  const user = Object.assign({}, emp);

  user.id = {
    dir: oracledb.BIND_OUT,
    type: oracledb.NUMBER
  }
  const result = await database.simpleExecute(createSQL, user);

  user.id = result.outBinds(id[0]);

  return user;
}

async function find(context) {
  let query = baseQuery;
  const binds = {};
 
  if (context.id) {
    binds.id = context.id;
 
     query += `\nwhere id = :id`;
  }
 
  const result = await database.simpleExecute(query, binds);
 
  return result.rows;
}
 
module.exports.find = find;
module.exports.create = find;